#!/bin/bash
set -e
set -x

WORK_PATH='/srv'
SITE_PACKAGES_PATH="${WORK_PATH}/venv/runtime/lib/python3.11/site-packages"
SETTINGS_PATH="${SITE_PACKAGES_PATH}/dogproject/settings.py"
DATA_PROVISION_PATH='/dogconfig.d'
PROVISION_CONFIG_PATH="${DATA_PROVISION_PATH}/dogconfig"
PROVISION_DATABASE_PATH="${DATA_PROVISION_PATH}/DOG-database"
VENV_PATH="${WORK_PATH}/venv/runtime/bin/activate"


echo "Activating runtime Venv"
# shellcheck source=/srv/venv/runtime/bin/activate
. "${VENV_PATH}"
echo "Venv activated"

cd -- "${WORK_PATH}"
if ! [ -d "${WORK_PATH}/database" ]; then
	mkdir -p "${WORK_PATH}/database"
fi

# Init Django config
if [ ! -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" ]; then
    echo "DOG configuration template not supplied. Using source default."
else
    if [ -z "${DJANGO_DEBUG}" ]; then
    	echo "##### DJANGO_DEBUG NOT SET #####"
    elif [ -z "${DOGAPI_NETLOC}" ]; then
    	echo "##### DOGAPI_NETLOC NOT SET #####"
    elif [ -z "${DJANGO_PIWIK_WEBSITE_ID}" ]; then 
    	echo "##### DJANGO_PIWIK_WEBSITE_ID NOT SET #####"
    elif [ -z "${DJANGO_SECRET_KEY}" ]; then
    	echo "##### DJANGO_SECRET_KEY NOT SET #####"
    elif [ -z "${SERVERNAME}" ]; then
    	echo "##### SERVERNAME NOT SET #####"; 
    elif [ -z "${VERIFY_SSL}" ]; then
    	echo "##### VERIFY_SSL NOT SET #####";
    elif [ -z "${DTR_ENABLED}" ]; then
    	echo "##### DTR_ENABLED NOT SET #####";
    else
    sigil -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" \
    	   debug="${DJANGO_DEBUG:?}" \
   	   dogapi_netloc="${DOGAPI_NETLOC:?}" \
   	   piwik_website_id="${DJANGO_PIWIK_WEBSITE_ID:?}" \
   	   secret_key="${DJANGO_SECRET_KEY:?}" \
   	   servername="${SERVERNAME:?}" \
   	   verify_ssl="${VERIFY_SSL:?}" \
   	   dtr_enabled="${DTR_ENABLED:?}" > "${SETTINGS_PATH}"
    fi
fi

# If no data provided (1) load fixtures as initial values, otherwise (0)
DB_FROM_SCRATCH=1
# Initialize database
if [ -d "${PROVISION_DATABASE_PATH}" ]; then
    if [ "$(find ${PROVISION_DATABASE_PATH} -name \*.sqlite -type f  | wc -l)" -gt 0 ]; then
        echo -n "Trying to copy supplied database files... "
        # Check if db target location exists
        if [ "$(find ${WORK_PATH}/database/ -name database.sqlite -type f  | wc -l)" -gt 0 ]; then
            DB_FROM_SCRATCH=0
            echo "There is already a database file installed! Leaving it intact."
        else
            # Copy supplied database on fresh start
            cp "$(unset -v latest; for file in "${PROVISION_DATABASE_PATH}"/*.sqlite; do [ ! -L "$file" ] && [[ "$file" -nt "$latest" ]] &&  latest=$file; done; echo  "$latest")" "${WORK_PATH}/database/database    >  \.sqlite"
            chown 100:101 "${WORK_PATH}"/database/database.sqlite
            DB_FROM_SCRATCH=0
            echo "Done."
        fi
    fi
fi

# Migrate to target migration if needed
# Migrations should have been committed to Git repo for Centre Registry app!
python3 -m "django" migrate
# If no data source been spotted (backup, persisting volume data) load fixtures
# as initial data
if [ ${DB_FROM_SCRATCH} -eq 1 ]; then
    echo "WARNING: Initializing container from scratch, DOG has no source's test fixtures."
fi
echo "Collecting static files"
python3 -m "django" collectstatic --clear --no-input

echo "WARNING: Initializing cache, can take a while"
python3 -m "django" cache_refresh
echo "Cache initialised"

