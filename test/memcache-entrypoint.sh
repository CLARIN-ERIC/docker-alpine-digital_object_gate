#!/bin/ash
# shellcheck shell=dash

memcached -m 64 -A & 
while [ ! -f  "/test/done" ]; 
	do echo "Waiting for tests to finish"; 
	sleep 1; 
done; 
echo "shutdown" && exit 0

